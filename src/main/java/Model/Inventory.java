package Model;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
/**
 * Class that represents Inventory of the player.
 * @author Chaban Yevhen
 */
public class Inventory {
    public ArrayList<Item> inventory = new ArrayList<>();
    public int numberOfElements;

    public Inventory(){
        Item item = new Item();
        item.setDefault();
        inventory.add(item);
        inventory.add(item);
        inventory.add(item);
    }

    public void loadInventory() throws FileNotFoundException {
        File myObj = new File("src/main/resources/Inventory.txt");
        String data;
        numberOfElements = 0;
        Scanner myReader = new Scanner(myObj);
        while (myReader.hasNextLine()) {
            data = myReader.nextLine();
            if(data.substring(5, 6).equals("S")){
                Item item = new Item();
                item.setSword();
                item.count = Integer.parseInt(data.substring(2, 3));
                inventory.set(Integer.parseInt(data.substring(0, 1)) - 1, item);
                numberOfElements++;
            } else if(data.substring(5, 6).equals("D")){
                Item item = new Item();
                item.setDefault();
                item.count = Integer.parseInt(data.substring(2, 3));
                inventory.set(Integer.parseInt(data.substring(0, 1)) - 1, item);
                //numberOfElements++;
            } else if(data.substring(5, 6).equals("A")){
                Item item = new Item();
                item.setApple();
                item.count = Integer.parseInt(data.substring(2, 3));
                inventory.set(Integer.parseInt(data.substring(0, 1)) - 1, item);
                numberOfElements++;
            } else if(data.substring(5, 6).equals("K")){
                Item item = new Item();
                item.setKey();
                item.count = Integer.parseInt(data.substring(2, 3));
                inventory.set(Integer.parseInt(data.substring(0, 1)) - 1, item);
                numberOfElements++;
            }

        }
        myReader.close();
    }

    public void writeToInventoryFile(String name, int count, int newCount, int itemSlot) throws IOException {
        File fileToBeModified = new File("src/main/resources/Inventory.txt");
        String oldString = itemSlot + " " + count + "  Default";
        String oldString1 = itemSlot + " " + count +  "  Sword";
        String oldString2 = itemSlot + " " + count +  "  Apple";
        String oldString3 = itemSlot + " " + count +  "  Key";
        String newString = itemSlot + " " + newCount + "  " + name;

        StringBuilder oldContent = new StringBuilder();
        BufferedReader reader = null;
        FileWriter writer = null;

        reader = new BufferedReader(new FileReader(fileToBeModified));
        String line = reader.readLine();

        while (line != null)
        {
            oldContent.append(line).append(System.lineSeparator());

            line = reader.readLine();
        }


        String newContent = oldContent.toString().replaceAll(oldString, newString);
        String newC = newContent.replaceAll(oldString1, newString);
        newContent = newC.replaceAll(oldString2, newString);
        newC = newContent.replaceAll(oldString3, newString);
        //Check_test
        System.out.println(newC);

        writer = new FileWriter(fileToBeModified);

        writer.write(newC);
        reader.close();
        writer.close();
    }


    public void drawInventory(Graphics g, int numSlot){
        Image image;
        int x = 0;
        int y = 0;
        String defenceString;

        image = new ImageIcon("src/main/resources/Inv.png").getImage();
        g.drawImage(image, x, y, null);

        for(int i =0; i < 3; i++){
            defenceString = Integer.toString(inventory.get(i).count);
            if(i == numSlot){
                image = new ImageIcon("src/main/resources/red.png").getImage();
                g.drawImage(image, x, y, null);
            }
            if(inventory.get(i).name.equals("Sword")){
                image = new ImageIcon("src/main/resources/SwordItem.png").getImage();
                g.drawImage(image, x + 35, y + 15, 65, 65, null);
            } else if(inventory.get(i).name.equals("Apple")){
                image = new ImageIcon("src/main/resources/appleInv.png").getImage();
                g.drawImage(image, x + 35, y + 15, 65, 65,null);
            } else if(inventory.get(i).name.equals("Key")){
                image = new ImageIcon("src/main/resources/superKey.png").getImage();
                g.drawImage(image, x + 35, y + 15, 65, 65,null);
            } else if(inventory.get(i).name.equals("Default")){
                image = new ImageIcon("src/main/resources/empty.png").getImage();
                g.drawImage(image, x, y, null);
                if(i == numSlot){
                    image = new ImageIcon("src/main/resources/red.png").getImage();
                    g.drawImage(image, x, y, null);
                }

            }
            //-80 y
            g.setColor(Color.BLUE);
            g.drawString(defenceString, x + 12, y + 90);
            x = x + 121;
        }


    }
    public int isItemInInventory(String name){
        for (int i = 0; i < inventory.size(); i++){
            if(inventory.get(i).name.equals(name)){
                return i;
            }
        }
        return -1;
    }

    public static class Item{
        public String name = null;
        int count = 0;
        int x = 0;
        int y = 0;
        int healthBonus = 0;
        int damage = 0;
        public int defence = 0;
        public int getX() {
            return x;
        }
        public int getY() {
            return y;
        }
        public int getDamage(){
            return damage;
        }
        public int getCount(){
            return count;
        }
        public void setSword(){
            name = "Sword";
            count = 1;
            healthBonus = 10;
            damage = 40;
            defence = 10;
        }
        public void setApple(){
            name = "Apple";
            count = 1;
            healthBonus = 40;
            damage = 0;
            defence = 5;
        }
        public void setKey(){
            name = "Key";
            count = 1;
            healthBonus = 0;
            damage = 0;
            defence = 0;
        }
        public void setDefault(){
            name = "Default";
            count = 0;
            healthBonus = 0;
            damage = 0;
            defence = 0;
        }
    }
}
