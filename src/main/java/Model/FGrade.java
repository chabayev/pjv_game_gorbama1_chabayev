package Model;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;


/**
 * Class that represents fGrade.
 * @author Chaban Yevhen
 */
public class FGrade{

    private int x;
    private int y;
    public int healthPoints;
    Image image;
    Image ballImage;

    private int path = 0;

    public int position = 0; // 0 = up, 1 = left, 2 = right, 3 = down, 4 up-left, 5 - up-right, 6 - down-left, 7 - down-right


    BufferedImage img;
    BufferedImage imageWalls;
    Random rand = new Random();
    public Ball ball;

    /**
     * This is constructor of the fGrade, which set fGrade coord and downoload image from the disk.
     * @param spawn_x  This is the x coord of fGrade spawn
     * @param spawn_y  This is the y coord of fGrade spawn
     */
    public FGrade(int spawn_x, int spawn_y){
        x = spawn_x;
        y = spawn_y;
        healthPoints = 100;
        ball = new Ball();
        try {
            imageWalls = ImageIO.read(new File("src/main/resources/back.png"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        image = new ImageIcon("src/main/resources/F.png").getImage();
        try {
            img = ImageIO.read(new File("src/main/resources/F.png"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }

    public void setHealthPoints(int healthPoints) {
        this.healthPoints = healthPoints;
    }



    public Image getTexture(){
        switch (position){
            case 0:
                return image;

            case 1:
                return rotateImage(((Math.PI)*3)/2);

            case 2:
                return rotateImage ((Math.PI)/2);

            case 3:
                return rotateImage(Math.PI);

            case 4:
                return rotateImage(((Math.PI)*7)/4);

            case 5:
                return rotateImage((Math.PI)/4);

            case 6:
                return rotateImage(((Math.PI)*5)/4);

            case 7:
                return rotateImage(((Math.PI)*3)/4);

        }
        return image;
    }

    public void setXAndY(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void setCoordOfPlayerForAttack(Player player){
        ball.xBall = ball.xStartOfShot = x;
        ball.yBall = ball.yStartOfShot = y;
        ball.xEndOfShot = player.getX();
        ball.yEndOfShot = player.getY();
    }

    private AffineTransform rotateClockwise90(BufferedImage source, double angle) {
        AffineTransform transform = new AffineTransform();
        transform.rotate(angle, source.getWidth()/2, source.getHeight()/2);
        double offset = (source.getWidth()-source.getHeight())/2;
        transform.translate(offset,offset);
        return transform;
    }

    /**
     * This method is used to rotate image.
     * @param angle  This is the angle of the rotation
     * @return Image This returns rotated image
     */
    public Image rotateImage(double angle){
        BufferedImage output = new BufferedImage(img.getHeight(), img.getWidth(), img.getType());
        AffineTransformOp op = new AffineTransformOp(rotateClockwise90(img, angle), AffineTransformOp.TYPE_BILINEAR);
        op.filter(img, output);
        return output;

    }

    public boolean checkDoors(int x, int y, int x_s, int y_s) throws IOException {
        String data;
        int coord_from_x = 0;
        int coord_end_x = 0;
        int coord_from_y = 0;
        int coord_end_y = 0;
        int endOfThePath = 0;
        int i;
        File myObj = new File("src/main/resources/Map.txt");
        Scanner myReader = new Scanner(myObj);
        while (myReader.hasNextLine()) {
            data = myReader.nextLine();
            if (data.substring(0, 2).equals("DO")) {
                for (i = 30; i < data.length(); i++) {
                    if (data.charAt(i) == 'x') {
                        coord_from_x = i + 1;
                    }
                    if (data.charAt(i) == ',') {
                        coord_end_x = i;
                        break;
                    }
                }
                for (i = i + 1; i < data.length(); i++) {
                    if (data.charAt(i) == 'y') {
                        coord_from_y = i + 1;
                    }
                    if (data.charAt(i) == ',') {
                        coord_end_y = i;
                        break;
                    }

                }
                int x_door = Integer.parseInt(data.substring(coord_from_x, coord_end_x));
                int y_door = Integer.parseInt(data.substring(coord_from_y, coord_end_y));
                if ((x > x_door - 15 && x < x_door + 61  && y > y_door && y < y_door + 26) && data.substring(coord_end_y + 2, coord_end_y + 3).equals("c")){
                    return false;
                }
                //weight of door = 41 height od door = 16
            }
        }
        myReader.close();
        return checkWalls(x, y, x_s, y_s);
    }

    public boolean checkWalls(int x, int y, int x_s, int y_s) {
        int c = 0;
        int c_1 = imageWalls.getRGB(4, 304);
        int red_1 = (c_1 & 0x00ff0000) >> 16;
        int green_1 = (c_1 & 0x0000ff00) >> 8;
        int blue_1 = c_1 & 0x000000ff;
        if(x < x_s){
            for(int i = x; i < x_s; i++){
                c = imageWalls.getRGB(i, y);
                int red = (c & 0x00ff0000) >> 16;
                int green = (c & 0x0000ff00) >> 8;
                int blue = c & 0x000000ff;
                if(red == red_1 && green == green_1 && blue == blue_1){
                    return false;
                }
            }
        } else if(x > x_s){
            for(int i = x_s; i < x; i++){
                c = imageWalls.getRGB(i, y);
                int red = (c & 0x00ff0000) >> 16;
                int green = (c & 0x0000ff00) >> 8;
                int blue = c & 0x000000ff;
                if(red == red_1 && green == green_1 && blue == blue_1){
                    return false;
                }
            }
        } else if( y < y_s){
            for(int i = y; i < y_s; i++){
                c = imageWalls.getRGB(x, i);
                int red = (c & 0x00ff0000) >> 16;
                int green = (c & 0x0000ff00) >> 8;
                int blue = c & 0x000000ff;
                if(red == red_1 && green == green_1 && blue == blue_1){
                    return false;
                }
            }
        } else if( y > y_s){
            for(int i = y_s; i < y; i++){
                c = imageWalls.getRGB(x, i);
                int red = (c & 0x00ff0000) >> 16;
                int green = (c & 0x0000ff00) >> 8;
                int blue = c & 0x000000ff;
                if(red == red_1 && green == green_1 && blue == blue_1){
                    return false;
                }
            }
        }
        return true;
    }

    public void makeMove() throws IOException {
        switch (path){
            case 2:
                position = 2;
                if(checkDoors(x  - 25, y, x, y)) {
                    x = x - 3;
                } else{
                    path = rand.nextInt(8) ;
                }
                break;
            case 1:
                position = 1;
                if(checkDoors(x  + 25, y, x, y )) {
                    x = x + 3;
                }
                else{
                    path = rand.nextInt(8) ;
                }
                break;
            case 0:
                position = 0;
                if(checkDoors(x, y - 25, x, y)) {
                    y = y - 3;
                }
                else{
                    path = rand.nextInt(8) ;
                }
                break;
            case 3:
                position = 3;
                if(checkDoors(x, y + 25, x, y)) {
                    y = y + 3;
                }
                else{
                    path = rand.nextInt(8) ;
                }
                break;
            case 4:
                position = 4;
                if(checkDoors(x  - 25, y, x, y )) {
                    x = x - 3;
                    if(checkDoors(x , y - 25, x, y )){
                        y = y - 3;
                    }
                    //y = y - 3;
                }
                else{
                    path = rand.nextInt(8) ;
                }
                break;
            case 6:
                position = 6;
                if(checkDoors(x  - 25, y, x, y)) {
                    x = x - 3;
                    if(checkDoors(x , y + 25, x, y )){
                        y = y + 3;
                    }
                } else{
                    path = rand.nextInt(8) ;
                }
                break;
            case 5:
                position = 5;
                if(checkDoors(x  + 25, y, x, y)) {
                    x = x + 3;
                    if(checkDoors(x , y - 25, x, y )){
                        y = y - 3;
                    }
                }  else{
                    path = rand.nextInt(8) ;
                }
                break;
            case 7:
                position = 7;
                if(checkDoors(x  + 25, y, x, y )) {
                    x = x + 3;
                    if(checkDoors(x , y + 25, x, y )){
                        y = y + 3;
                    }
                } else{
                    path = rand.nextInt(8) ;
                }
                break;
        }

    }

}

