package Model;

import javax.swing.*;
import java.awt.*;

/**
 * Class that represents fGrade attack bullet.
 * @author Chaban Yevhen
 */
public class Ball {
    public int state; // 0 - not exist on the map, 1 - drawing, 2 - cooldown

    int ballCooldown;
    Image imageOfBall;
    int ballDirection = 0;

    public int xStartOfShot;
    public int yStartOfShot;
    public int xBall;
    public int yBall;
    public int xEndOfShot;
    public int yEndOfShot;

    public Ball() {
        ballCooldown = 3;
        state = 0;
    }

    public int getXBall() {
        return xBall;
    }

    public int getYBall() {
        return yBall;
    }

    public Image getBallTexture() {
        imageOfBall = new ImageIcon("src/main/resources/ball.png").getImage();
        return imageOfBall;
    }

    public void setBallDirection() {
            if ((xStartOfShot < xEndOfShot && yStartOfShot < yEndOfShot) && (xEndOfShot - xStartOfShot < 200 && yEndOfShot - yStartOfShot < 200)) {
                if ((xEndOfShot - xStartOfShot > 20 || yEndOfShot - yStartOfShot > 20)) {
                    state = 1;
                    ballDirection = 0;
                    return;
                }
            } else if ((xStartOfShot < xEndOfShot && yStartOfShot > yEndOfShot) && (xEndOfShot - xStartOfShot < 200 && yStartOfShot - yEndOfShot < 200)) {
                if (xEndOfShot - xStartOfShot > 20 || yStartOfShot - yEndOfShot > 20) {
                    state = 1;
                    ballDirection = 1;
                    return;
                }
            } else if ((xStartOfShot > xEndOfShot && yStartOfShot < yEndOfShot) && (xStartOfShot - xEndOfShot < 200 && yEndOfShot - yStartOfShot < 200)) {
                if (xStartOfShot - xEndOfShot > 20 || yEndOfShot - yStartOfShot > 20) {
                    state = 1;
                    ballDirection = 2;
                    return;
                }
            } else if ((xStartOfShot > xEndOfShot && yStartOfShot > yEndOfShot) && (xStartOfShot - xEndOfShot < 200 && yStartOfShot - yEndOfShot < 200)) {
                if (xStartOfShot - xEndOfShot > 20 || yStartOfShot - yEndOfShot > 20) {
                    state = 1;
                    ballDirection = 3;
                    return;
                }
            }
        state = 0;
    }

    public boolean isShotFinished(Player player){
        if(player.getX() > xBall - 25 && player.getX() < xBall + 25 && player.getY() > yBall - 25 && player.getY() < yBall + 25){
            if (player.getArmor() > 0){
                player.setArmor(player.getArmor() - 5);
            } else {
                player.setHealthPoints(player.getHealthPoints() - 5);
            }
            return true;
        }
        switch (ballDirection){
            case 0:
                xBall = xBall + 2;
                yBall = yBall + 2;
                if(xBall >= xEndOfShot && yBall >= yEndOfShot){
                    ballCooldown = 0;
                    return true;
                }
                break;
            case 1:
                xBall = xBall + 2;
                yBall = yBall - 2;
                if(xBall >= xEndOfShot && yBall <= yEndOfShot){
                    ballCooldown = 0;
                    return true;
                }
                break;
            case 2:
                xBall = xBall - 2;
                yBall = yBall + 2;
                if(xBall <= xEndOfShot && yBall >= yEndOfShot){
                    ballCooldown = 0;
                    return true;
                }
                break;
            case 3:
                xBall = xBall - 2;
                yBall = yBall - 2;
                if(xBall <= xEndOfShot && yBall <= yEndOfShot){
                    ballCooldown = 0;
                    return true;
                }
                break;
        }
        return false;
    }

}
