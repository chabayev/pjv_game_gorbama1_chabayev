package View.HelpWithDrawing;

import Model.FGrade;
import Model.Player;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.*;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Class that represents map of the game.
 * @author Chaban Yevhen
 */
public class Map {
    String data;
    Image image;
    int endOfThePath;
    int coord_from_x;
    int coord_end_x;
    int coord_from_y;
    int coord_end_y;
    int i;
    BufferedImage img_door;
    public int gameover = 0;
    public boolean complete = false;

    /**
     * This method is used to draw map.
     * @param g This is the Graphical object for drawing
     */
    public void drawMap(Graphics g) throws FileNotFoundException {
        File myObj = new File("src/main/resources/Map.txt");
        Scanner myReader = new Scanner(myObj);
        while (myReader.hasNextLine()){
            data = myReader.nextLine();
            if(data.substring(0, 2).equals("BA")){
                image = new ImageIcon(data.substring(3,30)).getImage();
                g.drawImage(image, 0, 0, 1535, 841, null);
            } else if(data.substring(0, 2).equals("DO")){
                for(i = 30; i < data.length(); i++){
                    if(data.charAt(i) == 'x'){
                        coord_from_x = i + 1;
                    }
                    if(data.charAt(i) == ','){
                        coord_end_x = i;
                        break;
                    }
                }
                for(i = i + 1; i < data.length(); i++){
                    if(data.charAt(i) == 'y'){
                        coord_from_y = i + 1;
                    }
                    if(data.charAt(i) == ','){
                        coord_end_y = i;
                        break;
                    }
                }
                image = new ImageIcon(data.substring(3, 30)).getImage();
                int x = Integer.parseInt(data.substring(coord_from_x, coord_end_x));
                int y = Integer.parseInt(data.substring(coord_from_y, coord_end_y));
                if(data.substring(coord_end_y + 2, coord_end_y + 3).equals("c")){
                    g.drawImage(image, x, y,null);
                } else if(data.substring(coord_end_y + 2, coord_end_y + 3).equals("o")){
                    try {
                        img_door = ImageIO.read(new File("src/main/resources/door.png"));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    g.drawImage(rotateImage(Math.PI/2, img_door), x - 20, y + 15,null);
                }
            } else if(data.substring(0, 2).equals("GR")){

                for(i = 27; i < data.length(); i++){
                    if(data.charAt(i) == 'x'){
                        coord_from_x = i + 1;
                    }
                    if(data.charAt(i) == ','){
                        coord_end_x = i;
                        break;
                    }
                }
                for(i = i + 1; i < data.length(); i++){
                    if(data.charAt(i) == 'y'){
                        coord_from_y = i + 1;
                    }
                    if(data.charAt(i) == ','){
                        coord_end_y = i;
                        break;
                    }
                }
                if(data.substring(coord_end_y + 2, coord_end_y + 3).equals("n")){
                    image = new ImageIcon(data.substring(3, 27)).getImage();
                    int x = Integer.parseInt(data.substring(coord_from_x, coord_end_x));
                    int y = Integer.parseInt(data.substring(coord_from_y, coord_end_y));
                    g.drawImage(image, x, y,null);
                    }
                }
            else if(data.substring(0, 2).equals("IT")){
                for(i = 3; i < data.length();i++){
                    if(data.charAt(i) == 'g'){
                        endOfThePath = i+1;
                        break;
                    }
                }
                for(i = i; i < data.length(); i++){
                    if(data.charAt(i) == 'x'){
                        coord_from_x = i + 1;
                    }
                    if(data.charAt(i) == ','){
                        coord_end_x = i;
                        break;
                    }
                }
                for(i = i + 1; i < data.length(); i++){
                    if(data.charAt(i) == 'y'){
                        coord_from_y = i + 1;
                    }
                    if(data.charAt(i) == ','){
                        coord_end_y = i;
                        break;
                    }
                }
                if(data.substring(coord_end_y + 2, coord_end_y + 3).equals("n")){
                    image = new ImageIcon(data.substring(3, endOfThePath)).getImage();
                    int x = Integer.parseInt(data.substring(coord_from_x, coord_end_x));
                    int y = Integer.parseInt(data.substring(coord_from_y, coord_end_y));
                    g.drawImage(image, x, y, 30,40,null);

                }
            }
        }
        myReader.close();
    }

    /**
     * This method is used to open all doors on the map, and removes all fGrades.
     * @param player This is the player
     * @param grades  This is the arrayList of the grades on the map
     */
    public static void setSuperKey(Player player, ArrayList<FGrade> grades) throws IOException {
        File fileToBeModified = new File("src/main/resources/Map.txt");
        //player.collectedGrades.clear();
        player.setHealthPoints(999);
        player.setArmor(999);
        for(int i = 0; i < grades.size(); i++){
            grades.get(i).setHealthPoints(0);
        }
        //player.image = new ImageIcon("src/main/resources/Person.png").getImage();
        //player.img = ImageIO.read(new File("src/main/resources/Person.png"));
        String oldStringDoor = " c ";
        String newStringDoor = " o ";
        StringBuilder oldContent = new StringBuilder();
        BufferedReader reader = null;
        FileWriter writer = null;
        reader = new BufferedReader(new FileReader(fileToBeModified));
        String line = reader.readLine();
        while (line != null)
        {
            oldContent.append(line).append(System.lineSeparator());
            line = reader.readLine();
        }
        String newContent = oldContent.toString().replaceAll(oldStringDoor, newStringDoor);
        System.out.println(newContent);

        writer = new FileWriter(fileToBeModified);
        writer.write(newContent);

        reader.close();
        writer.close();
    }

    /**
     * This method is used reset map.
     * @param player This is the player
     */
    public static void setNewMap(Player player) throws IOException {
        File fileToBeModified = new File("src/main/resources/Map.txt");
        player.setX(958);
        player.setY(362);
        player.collectedGrades.clear();
        player.setHealthPoints(100);
        player.setArmor(0);
        player.setImages("src/main/resources/Person.png");
        //player.setImage(new ImageIcon("src/main/resources/Person.png").getImage());
        //player.img = ImageIO.read(new File("src/main/resources/Person.png"));
        String oldStringDoor = " o ";
        String oldStringGrade = " p";

        String newStringDoor = " c ";
        String newStringGrade = " n";

        StringBuilder oldContent = new StringBuilder();
        BufferedReader reader = null;
        FileWriter writer = null;

        reader = new BufferedReader(new FileReader(fileToBeModified));
        String line = reader.readLine();

        while (line != null)
        {
            oldContent.append(line).append(System.lineSeparator());

            line = reader.readLine();
        }


        String newContent = oldContent.toString().replaceAll(oldStringDoor, newStringDoor);
        String newCont = newContent.replaceAll(oldStringGrade, newStringGrade);
        //Check_test
        System.out.println(newCont);

        writer = new FileWriter(fileToBeModified);

        writer.write(newCont);
        reader.close();
        writer.close();
    }

    /**
     * This method is used to overwrite map.txt file and changed door status
     * @param letter This is the letter, what helps to detect door in the file
     */
    public void changeDoorStatus(char letter) throws IOException {
        File fileToBeModified = new File("src/main/resources/Map.txt");
        String oldString = "c " + letter;
        String newString = "o " + letter;

        StringBuilder oldContent = new StringBuilder();
        BufferedReader reader = null;
        FileWriter writer = null;

        reader = new BufferedReader(new FileReader(fileToBeModified));
        String line = reader.readLine();

        while (line != null)
        {
            oldContent.append(line).append(System.lineSeparator());

            line = reader.readLine();
        }


        String newContent = oldContent.toString().replaceAll(oldString, newString);
        //Check_test
        System.out.println(newContent);

        writer = new FileWriter(fileToBeModified);

        writer.write(newContent);
        reader.close();
        writer.close();

    }

    /**
     * This method is used to overwrite map.txt file and change grade status
     * @param x This is the xcoord of the grade to change
     * @param y  This is the ycoord of the grade to change
     */
    public void changeGradeStatus(int x, int y) throws IOException {
        File fileToBeModified = new File("src/main/resources/Map.txt");
        if( x == 122 && y == 775){
            gameover = 1;
        }
        String oldString = "x" + x +", y" + y + ", n";
        String newString = "x" + x +", y" + y + ", p";

        StringBuilder oldContent = new StringBuilder();
        BufferedReader reader = null;
        FileWriter writer = null;

        reader = new BufferedReader(new FileReader(fileToBeModified));
        String line = reader.readLine();

        while (line != null)
        {
            oldContent.append(line).append(System.lineSeparator());

            line = reader.readLine();
        }


        String newContent = oldContent.toString().replaceAll(oldString, newString);
        //Check_test
        System.out.println(newContent);

        writer = new FileWriter(fileToBeModified);

        writer.write(newContent);
        reader.close();
        writer.close();
    }


    private AffineTransform rotateClockwise90(BufferedImage source, double angle) {
        AffineTransform transform = new AffineTransform();
        transform.rotate(angle, source.getWidth()/2, source.getHeight()/2);
        double offset = (source.getWidth()-source.getHeight())/2;
        transform.translate(offset,offset);
        return transform;
    }

    /**
     * This method is used to rotate image.
     * @param img This is the image for rotation
     * @param angle  This is the angle of the rotation
     * @return Image This returns rotated image
     */
    public Image rotateImage(double angle, BufferedImage img){
        BufferedImage output = new BufferedImage(img.getHeight(), img.getWidth(), img.getType());
        AffineTransformOp op = new AffineTransformOp(rotateClockwise90(img, angle), AffineTransformOp.TYPE_BILINEAR);
        op.filter(img, output);
        return output;

        //BufferedImage rotatedImage = rotate.apply(image);

    }

}
