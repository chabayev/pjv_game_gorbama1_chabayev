package Model;



import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Class that represents player.
 * @author Chaban Yevhen
 */
public class Player {
    private int x;
    private int y;
    private int healthPoints;
    private int numberOfItemSlot = 0;
    private int damage;
    private int armor;

    public ArrayList<Character> collectedGrades = new ArrayList<>();
    public Inventory inventory = new Inventory();

    public boolean swordRotate;
    public boolean leftPunch;
    public boolean hasSword = false;
    public boolean setSuper = false;
    public boolean swordPunch = false;

    //String personGrades;


    private Image image;
    private BufferedImage img;

    private int position = 0; // 0 = up, 1 = left, 2 = right, 3 = down, 4 up-left, 5 - up-right, 6 - down-left, 7 - down-right. -1 = dead,, 8 - fight
    public boolean positionIsSet = false;

    ArrayList<String> animationOfFight = new ArrayList<>();

    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }
    public void setX(int x) {
        this.x = x;
    }
    public void setY(int y) {
        this.y = y;
    }

    public int getHealthPoints() {
        return healthPoints;
    }
    public void setHealthPoints(int healthPoints) {
        this.healthPoints = healthPoints;
    }

    public int getArmor() {
        return armor;
    }
    public void setArmor(int armor) {
        this.armor = armor;
    }

    public int getNumberOfItemSlot() {
        return numberOfItemSlot;
    }
    public void setNumberOfItemSlot(int numberOfItemSlot) {
        this.numberOfItemSlot = numberOfItemSlot;
    }

    public int getDamage() {
        return damage;
    }

    public int getPosition() {
        return position;
    }
    public void setPosition(int position) {
        this.position = position;
    }

    public Image getTexture(){
        switch (position){
            case 0:
                return image;
            case 1:
                return rotateImage(((Math.PI)*3)/2);

            case 2:
                return rotateImage ((Math.PI)/2);

            case 3:
                return rotateImage(Math.PI);

            case 4:
                return rotateImage(((Math.PI)*7)/4);

            case 5:
                return rotateImage((Math.PI)/4);

            case 6:
                return rotateImage(((Math.PI)*5)/4);

            case 7:
                return rotateImage(((Math.PI)*3)/4);
            case -1:
                image = new ImageIcon("src/main/resources/dead.png").getImage();
                break;
        }
        return image;
    }

    public void useItem() throws IOException {
        if(inventory.inventory.get(numberOfItemSlot).count > 0) {
            if (inventory.inventory.get(numberOfItemSlot).name.equals("Key")) {
                    if(numberOfItemSlot == 2){
                        inventory.writeToInventoryFile("Default", 1, 0, 3);
                        inventory.numberOfElements--;
                    } else{
                        inventory.writeToInventoryFile(inventory.inventory.get(numberOfItemSlot + 1).name, inventory.inventory.get(numberOfItemSlot).count, inventory.inventory.get(numberOfItemSlot + 1).count, numberOfItemSlot + 1);
                        inventory.writeToInventoryFile("Default", inventory.inventory.get(numberOfItemSlot + 1).count, 0, numberOfItemSlot + 2);
                        inventory.numberOfElements--;
                    }
                setSuper = true;
            } else {
                healthPoints = healthPoints + inventory.inventory.get(numberOfItemSlot).healthBonus;
                armor = armor + inventory.inventory.get(numberOfItemSlot).defence;
                    if(numberOfItemSlot == 2){
                        if(inventory.inventory.get(numberOfItemSlot).count == 1){
                            inventory.writeToInventoryFile("Default", 1, 0, 3);
                        } else{
                            inventory.writeToInventoryFile(inventory.inventory.get(numberOfItemSlot).name, inventory.inventory.get(numberOfItemSlot).count, inventory.inventory.get(numberOfItemSlot).count - 1, numberOfItemSlot + 1);
                        }

                        inventory.numberOfElements--;
                    } else{
                        if(inventory.inventory.get(numberOfItemSlot).count == 1){
                            inventory.writeToInventoryFile(inventory.inventory.get(numberOfItemSlot + 1).name, inventory.inventory.get(numberOfItemSlot).count, inventory.inventory.get(numberOfItemSlot + 1).count, numberOfItemSlot + 1);
                            inventory.writeToInventoryFile("Default", inventory.inventory.get(numberOfItemSlot + 1).count, 0, numberOfItemSlot + 2);
                        } else{
                            inventory.writeToInventoryFile(inventory.inventory.get(numberOfItemSlot).name, inventory.inventory.get(numberOfItemSlot).count, inventory.inventory.get(numberOfItemSlot).count - 1, numberOfItemSlot + 1);
                        }


                        inventory.numberOfElements--;
                    }
            }
        }
    }

    public void checkInventory(){
        Inventory.Item currItem;
        currItem = inventory.inventory.get(numberOfItemSlot);
        if(currItem.name.equals("Sword")){
            setImages("src/main/resources/PersonSw1.png");
            hasSword = true;
        } else if(currItem.name.equals("Apple")){
            setImages("src/main/resources/PersonApple.png");
            hasSword = false;
        } else if(currItem.name.equals("Key")){
            setImages("src/main/resources/PersonKey.png");
            hasSword = false;
        } else if(currItem.name.equals("Default")){
            setImages("src/main/resources/Person.png");
            hasSword = false;
        }
        //healthPoints = healthPoints + currItem.healthBonus;
        //armor = armor + currItem.armor;
        //damage = damage + currItem.getDamage();

        /*
        for(int i = 0; i < inventory.inventory.size(); i++){
            currItem = inventory.inventory.get(i);
            if(currItem.name.equals("Sword")){
                if(!hasSword && currItem.count == 0) {
                    healthPoints = healthPoints + currItem.healthBonus;
                    armor = armor + currItem.armor;
                    damage = damage + currItem.getDamage();
                    hasSword = true;
                }
            } else if(currItem.name.equals("Apple")){
                if(!hasApple) {
                    healthPoints = healthPoints + currItem.healthBonus;
                    armor = armor + currItem.armor;
                    damage = damage + currItem.getDamage();
                    hasApple = true;
                }
            }else if(currItem.name.equals("Key")){
                if(!hasKey) {
                    healthPoints = healthPoints + currItem.healthBonus;
                    armor = armor + currItem.armor;
                    damage = damage + currItem.getDamage();
                    hasKey = true;
                }
            }else if(currItem.name.equals("Defender")){
                if(!hasDefender) {
                    healthPoints = healthPoints + currItem.healthBonus;
                    armor = armor + currItem.armor;
                    damage = damage + currItem.getDamage();
                    hasDefender = true;
                }
            }
        }

         */
    }

    public Player(){
        this.x = 958;
        this.y = 362;
        animationOfFight.add(0, "src/main/resources/PersonSw1.png");
        animationOfFight.add(1, "src/main/resources/PersonSw2.png");
        animationOfFight.add(2, "src/main/resources/PersonSw3.png");
        damage = 5;
        armor = 0;
        healthPoints = 100;
        try {
            fillCollectedGrades();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void setImages(String string){
        image = new ImageIcon(string).getImage();
        try {
            img = ImageIO.read(new File(string));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void fillCollectedGrades() throws FileNotFoundException {
        File myObj = new File("src/main/resources/Map.txt");
        Scanner myReader = new Scanner(myObj);
        String data;
        while (myReader.hasNextLine()){
            data = myReader.nextLine();
            if(data.substring(0, 2).equals("GR")){

                if(data.charAt(data.length() - 1) == 'p'){
                    collectedGrades.add(data.charAt(22));
                } else if(data.charAt(data.length() - 1) == 'n'){
                    if(collectedGrades.contains(data.charAt(22))){
                        collectedGrades.remove(data.charAt(22));
                    }
                }
            }
        }
        myReader.close();
    }

    private AffineTransform rotateClockwise90(BufferedImage source, double angle) {
        AffineTransform transform = new AffineTransform();
        transform.rotate(angle, source.getWidth()/2, source.getHeight()/2);
        double offset = (source.getWidth()-source.getHeight())/2;
        transform.translate(offset,offset);
        return transform;
    }

    private Image rotateImage(double angle){
        BufferedImage output = new BufferedImage(img.getHeight(), img.getWidth(), img.getType());
        AffineTransformOp op = new AffineTransformOp(rotateClockwise90(img, angle), AffineTransformOp.TYPE_BILINEAR);
        op.filter(img, output);
        return output;

        //BufferedImage rotatedImage = rotate.apply(image);

    }


}
