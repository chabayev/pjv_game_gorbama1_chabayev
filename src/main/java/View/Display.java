package View;

import View.HelpWithDrawing.Panel;

import javax.swing.JFrame;
import java.io.FileNotFoundException;
import java.io.IOException;
/**
 * Class that represents Display, which contains panel.
 * @author Chaban Yevhen
 */
public class Display extends JFrame implements Runnable{

    public Panel mainPanel;

    public Display() throws IOException {
        setSize(1920, 1080);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        mainPanel = new Panel();
        add(mainPanel);
        pack();
        setVisible(true);
    }

    public void run() {
        boolean running = true;
        while(running){
            if(mainPanel.currentState == 1){
                try {
                    mainPanel.gameSet();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mainPanel.update();
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

        } else if(mainPanel.currentState == 0){
                mainPanel.update();
            } else if(mainPanel.currentState == -1){
                running = false;
            }
        }
        System.exit(0);
    }
}
