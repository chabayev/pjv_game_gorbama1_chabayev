package Controller;
import Model.FGrade;
import View.Display;

import java.io.IOException;

/**
 * Class that starts JFrame.
 * @author Chaban Yevhen
 */
public class GameStart {
    public static void main(String[] args) throws IOException {
        startJFrame();
    }
    public static void startJFrame() throws IOException {
        Thread childTread = new Thread(new Display());
        childTread.start();
    }
}
