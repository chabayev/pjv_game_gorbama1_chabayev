package View.HelpWithDrawing;


import Model.FGrade;
import Model.Inventory;
import Model.Player;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import javax.imageio.ImageIO;
import javax.swing.*;

/**
 * Class that represents main Panel of the game, which displays everything.
 * @author Chaban Yevhen
 */

public class Panel extends JPanel {


    //Images
    //-------------------------------------------------
    //
    BufferedImage imageWalls;
    Image image;


    //States
    //-------------------------------------------------
    //
    public int currentState = 0;
    int menuState = 0;
    int exitState = -1;
    int gameState = 1;
    int winState = 2;


    //Menu buttons
    //-------------------------------------------------
    //
    JButton buttonStart;
    JButton buttonContinue;
    JButton buttonExit;
    JButton buttonRestart;


    //State of the control keys
    //-------------------------------------------------
    //
    ArrayList<Boolean> pressedKeys = new ArrayList<Boolean>();
    boolean w = false;
    boolean a = false;
    boolean s = false;
    boolean d = false;

    boolean setNewMap = false;
    boolean winGame = false;

    //Objects of the game
    //-------------------------------------------------
    //
    Map map = new Map();

    Player player;
    FGrade fGrade;
    FGrade fGrade_2;
    FGrade fGrade_3;
    FGrade fGrade_4;
    FGrade fGrade_5;
    FGrade fGrade_6;
    FGrade fGrade_7;

    int animationOfPunch = 0;
    int numOfAnimationForRotation = 0;

    ArrayList<FGrade> fGrades = new ArrayList<>();

    /**This is constructor of the Panel, which set buttons, listeners and adding fGrades to the game.
     */
    public Panel(){
        // pressedKeys = ( (w, false), (a, false), (s, false), (d, false))
        setLayout(null);
        pressedKeys.add(w);
        pressedKeys.add(a);
        pressedKeys.add(s);
        pressedKeys.add(d);
        try {
            imageWalls = ImageIO.read(new File("src/main/resources/back.png"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        addKeyListener(new Panel.myKeyListener());
        setBounds(0, 0, 1920, 1080);
        setFocusable(true);
        requestFocusInWindow();
        buttonStart = new JButton(new ImageIcon("src/main/resources/Start.jpg"));
        buttonExit = new JButton(new ImageIcon("src/main/resources/Exit.png"));
        buttonStart.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //setNewMap = true;
                currentState = gameState;
            }
        });
        buttonExit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                currentState = exitState;
            }
        });
        buttonStart.setBounds(510, 384, 510, 71);
        buttonExit.setBounds(720, 584, 100, 31);


        player = new Player();
        player.setImages("src/main/resources/Person.png");
        fGrade =   new FGrade(970, 500);
        fGrade_2 = new FGrade(1246, 393);
        fGrade_3 = new FGrade(232, 600);
        fGrade_4 = new FGrade(184, 600);
        fGrade_5 = new FGrade(1390, 670);
        fGrade_6 = new FGrade(1395, 680);
        fGrade_7 = new FGrade(1400, 690);
        fGrade_5.setHealthPoints(300);
        fGrade_6.setHealthPoints(300);
        fGrade_7.setHealthPoints(300);

        fGrades.add(fGrade);
        fGrades.add(fGrade_2);
        fGrades.add(fGrade_3);
        fGrades.add(fGrade_4);
        fGrades.add(fGrade_5);
        fGrades.add(fGrade_6);
        fGrades.add(fGrade_7);
    }

    /**
     * This method is used to repaint panel.
     */
    public void update() {
        repaint();
    }

    /**
     * This method is used to check if player move is valid and check if player pick up something.
     * @param x This is the xcoord of the player to check
     * @param y  This is the ycoord of the player to check
     * @param x_s This is the current xcoord of the player
     * @param y_s  This is the current ycoord of the player
     * @return boolean This returns true if move is valid.
     */
    public boolean checkDoorsItemsOrGrades(int x, int y, int x_s, int y_s) throws IOException {
        String data;
        int coord_from_x = 0;
        int coord_end_x = 0;
        int coord_from_y = 0;
        int coord_end_y = 0;
        int endOfThePath = 0;
        int i;
        File myObj = new File("src/main/resources/Map.txt");
        Scanner myReader = new Scanner(myObj);
        while (myReader.hasNextLine()) {
            data = myReader.nextLine();
            if (data.substring(0, 2).equals("DO")) {
                for (i = 30; i < data.length(); i++) {
                    if (data.charAt(i) == 'x') {
                        coord_from_x = i + 1;
                    }
                    if (data.charAt(i) == ',') {
                        coord_end_x = i;
                        break;
                    }
                }
                for (i = i + 1; i < data.length(); i++) {
                    if (data.charAt(i) == 'y') {
                        coord_from_y = i + 1;
                    }
                    if (data.charAt(i) == ',') {
                        coord_end_y = i;
                        break;
                    }

                }
                int x_door = Integer.parseInt(data.substring(coord_from_x, coord_end_x));
                int y_door = Integer.parseInt(data.substring(coord_from_y, coord_end_y));
                if ((x > x_door - 15 && x < x_door + 61  && y > y_door && y < y_door + 26) && data.substring(coord_end_y + 2, coord_end_y + 3).equals("c")){
                    return false;
                }
                //weight of door = 41 height od door = 16
            } else if(data.substring(0, 2).equals("GR")){
                for(i = 27; i < data.length(); i++){
                    if(data.charAt(i) == 'x'){
                        coord_from_x = i + 1;
                    }
                    if(data.charAt(i) == ','){
                        coord_end_x = i;
                        break;
                    }
                }
                for(i = i + 1; i < data.length(); i++){
                    if(data.charAt(i) == 'y'){
                        coord_from_y = i + 1;
                    }
                    if(data.charAt(i) == ','){
                        coord_end_y = i;
                        break;
                    }
                }
                int x_door = Integer.parseInt(data.substring(coord_from_x, coord_end_x));
                int y_door = Integer.parseInt(data.substring(coord_from_y, coord_end_y));
                if ((player.getX() > x_door - 30 && player.getX() < x_door + 30  && player.getY() > y_door - 30 && player.getY() < y_door + 30) && data.substring(coord_end_y + 2, coord_end_y + 3).equals("n")){
                    player.collectedGrades.add(data.charAt(22));
                    map.changeGradeStatus(x_door, y_door);
                    map.changeDoorStatus(data.charAt(22));
                    if(data.charAt(22) == 'A'){
                        currentState = winState;
                        map.complete = true;
                    }
                }
            } else if(data.substring(0, 2).equals("IT")){
                    Inventory.Item item = null;
                    for(i = 3; i < data.length();i++){
                        if(data.charAt(i) == 'g'){
                            endOfThePath = i+1;
                            break;
                        }
                    }
                    for(i = endOfThePath; i < data.length(); i++){
                        if(data.charAt(i) == 'x'){
                            coord_from_x = i + 1;
                        }
                        if(data.charAt(i) == ','){
                            coord_end_x = i;
                            break;
                        }
                    }
                    for(i = i + 1; i < data.length(); i++){
                        if(data.charAt(i) == 'y'){
                            coord_from_y = i + 1;
                        }
                        if(data.charAt(i) == ','){
                            coord_end_y = i;
                            break;
                        }
                    }
                    int x_door = Integer.parseInt(data.substring(coord_from_x, coord_end_x));
                    int y_door = Integer.parseInt(data.substring(coord_from_y, coord_end_y));
                    if ((player.getX() > x_door - 30 && player.getX() < x_door + 30  && player.getY() > y_door - 30 && player.getY() < y_door + 30) && data.substring(coord_end_y + 2, coord_end_y + 3).equals("n")){
                        map.changeGradeStatus(x_door, y_door);
                        switch (data.substring(coord_end_y + 4, coord_end_y + 5)) {
                            case "S":
                                item = new Inventory.Item();
                                item.setSword();
                                player.hasSword = true;
                                break;
                            case "A":
                                item = new Inventory.Item();
                                item.setApple();
                                break;
                            case "K":
                                item = new Inventory.Item();
                                item.setKey();
                                break;
                            case "D":
                                item = null;
                                player.setArmor(player.getArmor() + 200);
                                break;
                        }
                        if(item!=null){
                            if(player.inventory.isItemInInventory(item.name) != -1){
                                int indx  = player.inventory.isItemInInventory(item.name);
                                //player.inventory.addItem(item);
                                player.inventory.writeToInventoryFile(item.name, player.inventory.inventory.get(indx).getCount(), player.inventory.inventory.get(indx).getCount() + 1, indx + 1);
                            } else{
                                if(player.inventory.numberOfElements != 3) {
                                    player.inventory.writeToInventoryFile(item.name, 0, 1, player.inventory.numberOfElements + 1);
                                    //player.inventory.inventory.set(player.inventory.numberOfElements, item);
                                    player.inventory.numberOfElements++;
                                }
                            }

                        }

                    }
            }
        }
        myReader.close();
        return checkWalls(x, y, x_s, y_s);
    }

    /**
     * This method is used to check if player move is valid.
     * @param x This is the xcoord of the player to check
     * @param y  This is the ycoord of the player to check
     * @param x_s This is the current xcoord of the player
     * @param y_s  This is the current ycoord of the player
     * @return boolean This returns true if move is valid.
     */
    public boolean checkWalls(int x, int y, int x_s, int y_s) {
        int c = 0;
        int c_1 = imageWalls.getRGB(4, 304);
        int red_1 = (c_1 & 0x00ff0000) >> 16;
        int green_1 = (c_1 & 0x0000ff00) >> 8;
        int blue_1 = c_1 & 0x000000ff;
        if(x < x_s){
            for(int i = x; i < x_s; i++){
                c = imageWalls.getRGB(i, y);
                int red = (c & 0x00ff0000) >> 16;
                int green = (c & 0x0000ff00) >> 8;
                int blue = c & 0x000000ff;
                if(red == red_1 && green == green_1 && blue == blue_1){
                    return false;
                }
            }
        } else if(x > x_s){
            for(int i = x_s; i < x; i++){
                c = imageWalls.getRGB(i, y);
                int red = (c & 0x00ff0000) >> 16;
                int green = (c & 0x0000ff00) >> 8;
                int blue = c & 0x000000ff;
                if(red == red_1 && green == green_1 && blue == blue_1){
                    return false;
                }
            }
        } else if( y < y_s){
            for(int i = y; i < y_s; i++){
                c = imageWalls.getRGB(x, i);
                int red = (c & 0x00ff0000) >> 16;
                int green = (c & 0x0000ff00) >> 8;
                int blue = c & 0x000000ff;
                if(red == red_1 && green == green_1 && blue == blue_1){
                    return false;
                }
            }
        } else if( y > y_s){
            for(int i = y_s; i < y; i++){
                c = imageWalls.getRGB(x, i);
                int red = (c & 0x00ff0000) >> 16;
                int green = (c & 0x0000ff00) >> 8;
                int blue = c & 0x000000ff;
                if(red == red_1 && green == green_1 && blue == blue_1){
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * This method is used to set new player position, which depends to pressed keys.
     */
    private void setCoordOfPlayer() throws IOException {
        if (pressedKeys.get(0)) {
            if (pressedKeys.get(1)) {
                player.setPosition(4);
                player.positionIsSet = true;
                if(checkDoorsItemsOrGrades(player.getX() - 12, player.getY(), player.getX(), player.getY())){
                    player.setX(player.getX() - 5);
                }
            } else if (pressedKeys.get(3)) {
                player.setPosition(5);
                player.positionIsSet = true;
                if(checkDoorsItemsOrGrades(player.getX() + 12, player.getY(), player.getX(), player.getY())) {
                    player.setX(player.getX() + 5);
                }
            }
            if(!player.positionIsSet){
                player.setPosition(0);
                player.positionIsSet = true;
            }
            if(checkDoorsItemsOrGrades(player.getX(), player.getY() - 16, player.getX(), player.getY())){
                player.setY(player.getY() - 8);
            }

        } else if (pressedKeys.get(2)) {
            if (pressedKeys.get(1)) {
                player.setPosition(6);
                player.positionIsSet = true;
                if(checkDoorsItemsOrGrades(player.getX() - 12, player.getY(), player.getX(), player.getY())){
                    player.setX(player.getX() - 5);
                }
            } else if (pressedKeys.get(3)) {
                player.setPosition(7);
                player.positionIsSet = true;
                if(checkDoorsItemsOrGrades(player.getX() + 12, player.getY(), player.getX(), player.getY())){
                    player.setX(player.getX() + 5);
                }
            }
            if(!player.positionIsSet){
                player.setPosition(3);
                player.positionIsSet = true;
            }
            if(checkDoorsItemsOrGrades(player.getX(), player.getY() + 16, player.getX(), player.getY())){
                player.setY(player.getY() + 8);
            }

        } else if (pressedKeys.get(1)) {
            if (pressedKeys.get(0)) {
                player.setPosition(4);
                player.positionIsSet = true;
                if(checkDoorsItemsOrGrades(player.getX(), player.getY() - 12, player.getX(), player.getY())){
                    player.setY(player.getY() - 5);
                }
            } else if (pressedKeys.get(2)) {
                player.setPosition(6);
                player.positionIsSet = true;
                if(checkDoorsItemsOrGrades(player.getX(), player.getY() + 12, player.getX(), player.getY())){
                    player.setY(player.getY() + 5);
                }
            }
            if(!player.positionIsSet){
                player.setPosition(1);
                player.positionIsSet = true;
            }
            if(checkDoorsItemsOrGrades(player.getX() - 16, player.getY(), player.getX(), player.getY())){
                player.setX(player.getX() - 8);
            }
        } else if (pressedKeys.get(3)) {
            if (pressedKeys.get(0)) {
                player.setPosition(5);
                player.positionIsSet = true;
                if(checkDoorsItemsOrGrades(player.getX(), player.getY() - 12, player.getX(), player.getY())){
                    player.setY(player.getY() - 5);
                }
            } else if (pressedKeys.get(2)) {
                player.setPosition(7);
                player.positionIsSet = true;
                if(checkDoorsItemsOrGrades(player.getX(), player.getY() + 12, player.getX(), player.getY())){
                    player.setY(player.getY() + 5);
                }
            }
            if(!player.positionIsSet){
                player.setPosition(2);
                player.positionIsSet = true;
            }
            if(checkDoorsItemsOrGrades(player.getX() + 16, player.getY(), player.getX(), player.getY())){
                player.setX(player.getX() + 8);
            }
        }
    }

    /**
     * This method is used to fGrade attacking.
     * @param f This is the grade, which will attack
     */
    public void fGradeAttack(FGrade f){
        if(f.ball.state == 0){
            f.setCoordOfPlayerForAttack(player);
            f.ball.setBallDirection();
        } else if(f.ball.state == 1){
            if(f.ball.isShotFinished(player)){
                f.ball.state = 0;
            }
        }
    }

    /**
     * This method is used to fGrade moving.
     */
    public void fGradeMove() throws IOException {
        for(int i = 0; i < fGrades.size(); i++){
            fGrades.get(i).makeMove();
            fGradeAttack(fGrades.get(i));
        }
    }

    /**
     * This method is used set coord of player, check player inventory and make move of fGrades.
     */
    public void gameSet() throws IOException {
        setCoordOfPlayer();
        player.checkInventory();
        if(fGrades != null) {
            fGradeMove();
        }
    }

    /**
     * This class is used to listening keys.
     */
    public class myKeyListener implements KeyListener {
        @Override
        public void keyTyped(KeyEvent e) {
            if (e.getKeyChar() == 'r') {
                setNewMap = true;
                map.complete = false;
            }
            if (e.getKeyChar() == 'i') {
                player.leftPunch = true;
            }
            if (e.getKeyChar() == '1') {
                player.setNumberOfItemSlot(0);
                /*
                if(player.inventory.isItemInInventory("Sword")){
                    player.hasSword = true;
                    player.setImages("src/main/resources/PersonSw1.png");
                }

                 */
            }
            if (e.getKeyChar() == '2') {
                player.hasSword = false;
                player.setNumberOfItemSlot(1);
            }
            if (e.getKeyChar() == '3') {
                player.hasSword = false;
                player.setNumberOfItemSlot(2);
            }
            if (e.getKeyChar() == 'p') {
                if(player.hasSword && player.inventory.isItemInInventory("Sword") != -1){
                    player.swordPunch = true;
                } else{
                    try {
                        player.useItem();
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
            }
            if (e.getKeyChar() == 'o') {
                if(player.hasSword && player.inventory.isItemInInventory("Sword") != -1){
                    player.swordRotate = true;
                } else{
                    try {
                        player.useItem();
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
            }
        }
        @Override
        public void keyPressed(KeyEvent event) {
            switch (event.getKeyChar()) {
                case 'w':
                    pressedKeys.set(0, true);
                    break;
                case 'a':
                    pressedKeys.set(1, true);
                    break;
                case 's':
                    pressedKeys.set(2, true);
                    break;
                case 'd':
                    pressedKeys.set(3, true);
                    break;
            }
        }

        @Override
        public void keyReleased(KeyEvent event) {
            switch (event.getKeyChar()) {
                case 'w':
                    pressedKeys.set(0, false);
                    break;
                case 'a':
                    pressedKeys.set(1, false);
                    break;
                case 's':
                    pressedKeys.set(2, false);
                    break;
                case 'd':
                    pressedKeys.set(3, false);
                    break;
            }
        }
    }

    /**
     * This method is used to draw fireball, when fGrade attack.
     * @param g This is the Graphical object for painting
     * @param f  This is the attacking fGrade
     */
    public void drawBalls(Graphics g, FGrade f){
        if(f.ball.state == 1){
            g.drawImage(f.ball.getBallTexture(), f.ball.getXBall() - 28, f.ball.getYBall() - 24, null);
        }
    }

    /**
     * This method is used to draw map and everything else.
     */
    public void paintComponent(Graphics g){
        ArrayList<Integer> animationOfRotate = new ArrayList<Integer>();
        for(int i = 0; i < 8; i++){
            animationOfRotate.add(i);
        }
        if (currentState == menuState) {
            add(buttonStart);
            add(buttonExit);
            super.paintComponent(g);
            image = new ImageIcon("src/main/resources/Kampus.jpg").getImage();
            g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
        } else if (currentState == gameState) {
            if (setNewMap) {
                try {
                    Map.setNewMap(player);
                    player.inventory.writeToInventoryFile("Default", player.inventory.inventory.get(0).getCount(), 0, 1);
                    player.inventory.writeToInventoryFile("Default", player.inventory.inventory.get(1).getCount(), 0, 2);
                    player.inventory.writeToInventoryFile("Default", player.inventory.inventory.get(2).getCount(), 0, 3);
                    fGrades.clear();
                    fGrades.add(fGrade);
                    fGrades.add(fGrade_2);
                    fGrades.add(fGrade_3);
                    fGrades.add(fGrade_4);
                    for(int i  = 0; i < fGrades.size(); i++){
                        fGrades.get(i).setHealthPoints(100);
                    }
                    fGrades.add(fGrade_5);
                    fGrades.add(fGrade_6);
                    fGrades.add(fGrade_7);
                    fGrade_5.setHealthPoints(300);
                    fGrade_6.setHealthPoints(300);
                    fGrade_7.setHealthPoints(300);
                    fGrade.setXAndY(970, 500);
                    fGrade_2.setXAndY(1246, 393);
                    fGrade_3.setXAndY(232, 600);
                    fGrade_4.setXAndY(184, 600);
                    fGrade_5.setXAndY(1390, 670);
                    fGrade_6.setXAndY(1395, 680);
                    fGrade_7.setXAndY(1400, 690);


                    setNewMap = false;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                player.inventory.loadInventory();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            if (player.setSuper) {
                try {
                    Map.setSuperKey(player, fGrades);
                    player.setSuper = false;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            super.paintComponent(g);
            remove(buttonStart);
            remove(buttonExit);
            try {
                map.drawMap(g);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            //
            player.inventory.drawInventory(g, player.getNumberOfItemSlot());

            if (fGrades != null) {
                for (int i = 0; i < fGrades.size(); i++) {
                    if (fGrades.get(i).healthPoints <= 0) {
                        fGrades.remove(i);
                        i = i + 1;
                    }
                    if (player.getX() > fGrades.get(i).getX() - 30 && player.getX() < fGrades.get(i).getX() + 30 && player.getY() > fGrades.get(i).getY() - 30 && player.getY() < fGrades.get(i).getY() + 30) {
                        if (player.getArmor() > 0) {
                            player.setArmor(player.getArmor() - 1);
                        } else {
                            player.setHealthPoints(player.getHealthPoints() - 1);
                        }
                    }
                }
            }
            if (player.getHealthPoints() <= 0) {
                //player.die();
                setNewMap = true;
            }
            if (player.swordPunch) {
                if (animationOfPunch == 0) {
                    player.setImages("src/main/resources/PersonSw2.png");
                    animationOfPunch++;
                } else {
                    if(fGrades != null) {
                        for (FGrade grade : fGrades) {
                            checkFGradeNearPlayer(player, grade, player.getDamage());
                        }
                    }
                    player.setImages("src/main/resources/PersonSw3.png");
                    animationOfPunch = 0;
                    player.swordPunch = false;
                }
            } else if(player.swordRotate){
                player.setPosition(animationOfRotate.get(numOfAnimationForRotation));
                if(fGrades != null) {
                    for (FGrade grade : fGrades) {
                        checkFGradeNearPlayer(player, grade, player.getDamage() + 20);
                    }
                }
                numOfAnimationForRotation++;
                if(numOfAnimationForRotation == 8){
                    numOfAnimationForRotation = 0;
                    player.swordRotate = false;
                }
            }
            if (fGrades != null) {
                for (int i = 0; i < fGrades.size(); i++) {
                    if (fGrades.get(i).healthPoints <= 0) {
                        fGrades.remove(i);
                        i = i + 1;
                    } else {
                        drawBalls(g, fGrades.get(i));
                    }
                }
            }
            g.drawImage(player.getTexture(), player.getX() - 28, player.getY() - 24, null);
            player.positionIsSet = false;

            //Draw fgrades healthpoints
            if(fGrades!= null){
            for (int i = 0; i < fGrades.size(); i++) {
                g.drawImage(fGrades.get(i).getTexture(), fGrades.get(i).getX() - 21, fGrades.get(i).getY() - 21, null);
                if (fGrades.get(i).healthPoints < 25) {
                    g.setColor(Color.red);
                } else if (fGrades.get(i).healthPoints > 24 && fGrades.get(i).healthPoints < 75) {
                    g.setColor(Color.blue);
                } else if (fGrades.get(i).healthPoints > 74) {
                    g.setColor(Color.green);
                }

                g.drawString(String.valueOf(fGrades.get(i).healthPoints), fGrades.get(i).getX() - 21, fGrades.get(i).getY() - 21);
                }
            }

            //Draw player hp, armor and collected grades
            String defenceString =Integer.toString(player.getArmor());
            String healthpointString = Integer.toString(player.getHealthPoints());
            String collectedGradesString =player.collectedGrades.toString();

            //g.setColor(Color.black);
            //g.drawString("Press R to restart ", 720, 30);
            g.setColor(Color.red);
            g.drawString(healthpointString, 30, 165);
            g.drawString(defenceString, 150, 165);
            g.drawString(collectedGradesString, 235, 165);

        } else if(currentState == winState){
            setNewMap = true;
            currentState = menuState;
        }
    }

    /**
     * This method is used to check if player can attack fGRade or fGrade too far. If fGrade isn't too far, then hp of the fGrade will decrease.
     * @param player This is the player
     * @param damage  This is the players damage
     * @param fGrade This is the fGrade for check
     */
    public void checkFGradeNearPlayer(Player player, FGrade fGrade, int damage){
            switch (player.getPosition()){
                case 0:// up
                    if(player.getX() > fGrade.getX() - 20 && player.getX() < fGrade.getX() + 20 && (player.getY() - fGrade.getY()) < 60 && (player.getY() - fGrade.getY()) > 5){
                        fGrade.healthPoints = fGrade.healthPoints - damage;
                    }
                    break;
                case 1: //left
                    if(Math.abs(fGrade.getX() - player.getX()) > 5 && Math.abs(fGrade.getX() - player.getX()) < 60 && (player.getY() < fGrade.getY() + 20 && player.getY() > fGrade.getY() - 20)){
                        fGrade.healthPoints = fGrade.healthPoints - damage;
                    }
                    break;
                case 2://right
                    if(Math.abs(player.getX() - fGrade.getX()) > 5 && Math.abs(player.getX() - fGrade.getX()) < 60 && (player.getY() < fGrade.getY() + 20 && player.getY() > fGrade.getY() - 20)){
                        fGrade.healthPoints = fGrade.healthPoints - damage;
                    }
                    break;
                case 3://down
                    if(player.getX() > fGrade.getX() - 20 && player.getX() < fGrade.getX() + 20 && Math.abs(fGrade.getY() - player.getY()) < 60 && Math.abs(fGrade.getY() - player.getY()) > 5){
                        fGrade.healthPoints = fGrade.healthPoints - damage;
                    }
                    break;

                case 4://up-left
                    if((player.getY() - fGrade.getY()) < 60 && (player.getY() - fGrade.getY()) > 5 && Math.abs(fGrade.getX() - player.getX()) > 5 && Math.abs(fGrade.getX() - player.getX()) < 60){
                        fGrade.healthPoints = fGrade.healthPoints - damage;
                    }
                    break;

                case 5://up-right
                    if((player.getY() - fGrade.getY()) < 60 && (player.getY() - fGrade.getY()) > 5 && Math.abs(player.getX() - fGrade.getX()) > 5 && Math.abs(player.getX() - fGrade.getX()) < 60 ){
                        fGrade.healthPoints = fGrade.healthPoints - damage;
                    }
                    break;

                case 6://down-left
                    if(Math.abs(fGrade.getY() - player.getY()) < 60 && Math.abs(fGrade.getY() - player.getY()) > 5 && Math.abs(fGrade.getX() - player.getX()) > 5 && Math.abs(fGrade.getX() - player.getX()) < 60 ){
                        fGrade.healthPoints = fGrade.healthPoints - damage;
                    }
                    break;

                case 7://downright
                    if(Math.abs(fGrade.getY() - player.getY()) < 60 && Math.abs(fGrade.getY() - player.getY()) > 5 && Math.abs(player.getX() - fGrade.getX()) > 5 && Math.abs(player.getX() - fGrade.getX()) < 60){
                        fGrade.healthPoints = fGrade.healthPoints - damage;
                    }
                    break;

                     
            }


    }
}
